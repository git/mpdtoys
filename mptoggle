#!/usr/bin/perl
use strict;
use warnings;
use Audio::MPD q{0.19.0};

=head1 NAME

mptoggle - single button control for mpd

=head1 SYNOPSIS

mptoggle [host] [playlist ...]

=head1 DESCRIPTION

B<mptoggle> allows mpd to be controlled by a single physical button. It 
was designed for a linksys nslu2 running mpd, for which the only available
physical control is a power button that can be remapped to run an arbitrary
program.

B<mptoggle> toggles playback each time it's run. So press the button once
to start, and a second time to stop. Each time it starts playing. By
default it selects a new different playlist, at random, to play. If
playlist names are specified at the command line, it will choose between
those at random.

Example of how to make the nslu2's power button run mptoggle, in
/etc/inittab:

	ca:12345:ctrlaltdel:/usr/bin/mptoggle localhost

=head1 SEE ALSO

mprompt(1) mpd(1)

=head1 AUTHOR

Copyright 2007 Joey Hess <joey@kitenet.net>

Licensed under the GNU GPL version 2 or higher.

http://kitenet.net/~joey/code/mpdtoys

=cut

if (@ARGV) {
	$ENV{MPD_HOST}=shift;
}
my $mpd=Audio::MPD->new(conntype => "reuse");

if ($mpd->status->state ne 'play') {
	$mpd->playlist->clear;
	my @playlists;
	if (@ARGV) {
		@playlists=@ARGV;
	}
	else {
		@playlists=$mpd->collection->all_playlists;
	}
	$mpd->playlist->load($playlists[rand @playlists]);
	$mpd->play;
}
else {
	$mpd->pause;
}
